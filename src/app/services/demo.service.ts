import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const API_URL= environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class DemoService {

  constructor(private http: HttpClient) { }


  greeting(name: string) {
    return this.http.get(`${API_URL}/greeting/${name}`);
  }
}
