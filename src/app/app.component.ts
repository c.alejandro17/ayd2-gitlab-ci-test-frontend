import { Component } from '@angular/core';
import { DemoService } from './services/demo.service';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  message = '';
  form: FormGroup;

  constructor(private demoService: DemoService,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      name: ['']
    }); 
  }


  send() {
    let name = this.form.value.name;
    if (!name) {
      name = 'unnamed';
    }
    this.demoService.greeting(name)
      .subscribe( (resp: any) => {
        this.message = resp.data;
      });
  }


}
